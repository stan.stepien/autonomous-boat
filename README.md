# autonomous boat

#youtube videos
* https://youtu.be/GarDA7xUoPk
* https://youtu.be/NgIkgNb8BPI
* https://youtu.be/Di4KmLwCqrQ
* https://youtu.be/3-aY_-eYJ8M
 
# Budowa
* hull http://profs.scienze.univr.it/farinelli/intcatchai/intcatchai.html
* Raspberry -> arduino -> sterowniki silnikow i serw, czujniki
* Silniki i serwa
* Zasilanie osobne dla elektroniki i silników

# Tematy do opanowania
* Kształt i rodzaj kadłuba (rury pcv, styropian, aluminium, mi sie podoba kształt torpedy z drugiego linku, w razie awari silnika może popłynie na jednym)
(SeaCharger)[https://www.youtube.com/watch?v=xzv8TwyrJ38]
(SailDrone)[https://www.youtube.com/watch?v=iFTToTsJuY0]

* przeglad akumultorów
* Jak przewidywac czas pracy akumulatorów
* Obsługa gps i test jego dokładności w lesie i na jeziorze
* Jak użyć ardupilota lub coś podobnego?
* Łączność wifi lub gsm
* Kamera rpi podczerwona?

# image processing
https://github.com/gaia-boat/image-processing

